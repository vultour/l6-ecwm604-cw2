<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']    = 'main';
$route['404_override']          = '';
$route['translate_uri_dashes']  = FALSE;


$route["api/users"]["GET"]                  = "user/getall";
$route["api/users"]["POST"]                 = "user/create";
$route["api/users/(:num)"]["GET"]           = "user/get/$1";
$route["api/users/(:num)"]["PUT"]           = "user/update/$1";
$route["api/users/(:num)"]["DELETE"]        = "user/delete/$1";
$route["api/users/(:num)/posts"]["GET"]     = "post/userposts/$1";
$route["api/users/(:num)/comments"]["GET"]  = "comment/usercomments/$1";
$route["api/users/login"]["POST"]           = "user/login";
$route["api/users/logout"]["GET"]           = "user/logout";
$route["api/users/generate"]["GET"]         = "user/generate";
$route["api/users/current"]["GET"]          = "user/current";

$route["api/posts"]["GET"]                  = "post/getall";
$route["api/posts"]["POST"]                 = "post/create";
$route["api/posts/(:num)"]["GET"]           = "post/get/$1";
$route["api/posts/(:num)"]["PUT"]           = "post/update/$1";
$route["api/posts/(:num)"]["DELETE"]        = "post/delete/$1";
$route["api/posts/vote/(:num)"]["GET"]      = "post/getvote/$1";
$route["api/posts/vote/(:num)"]["PUT"]      = "post/putvote/$1";
$route["api/posts/sort"]["GET"]             = "post/getsort";
$route["api/posts/sort"]["POST"]            = "post/putsort";
$route["api/posts/generate"]["GET"]         = "post/generate";
$route["api/posts/simulate"]["GET"]         = "post/simulate";

$route["api/comments"]["GET"]               = "comment/getall";
$route["api/comments"]["POST"]              = "comment/create";
$route["api/comments/(:num)"]["GET"]        = "comment/get/$1";
$route["api/comments/(:num)"]["PUT"]        = "comment/update/$1";
$route["api/comments/(:num)"]["DELETE"]     = "comment/delete/$1";
$route["api/comments/vote/(:num)"]["GET"]   = "comment/getvote/$1";
$route["api/comments/vote/(:num)"]["PUT"]   = "comment/putvote/$1";
$route["api/comments/sort"]["GET"]          = "comment/getsort";
$route["api/comments/sort"]["POST"]         = "comment/putsort";
$route["api/comments/simulate"]["GET"]      = "comment/simulate";


$route["vote/(:num)/(:num)"]    = "main/vote/$1/$2";
$route["votes/(:num)"]          = "main/votes/$1";

$route["post/submit"]           = "main/submit";
$route["post/(:num)"]           = "main/post/$1";
$route["post/(:num)/(:any)"]    = "main/post/$1";

$route["submit/post"]           = "main/submitpost";
$route["submit/comment"]        = "main/submitcomment";

$route["sort/(:num)"]           = "main/sort/$1";

$route["(:num)"]                = "main/index/$1";
$route["(:any)"]                = "main/$1";