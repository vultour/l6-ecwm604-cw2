<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends EXT_Controller {
    public function __construct(){
        parent::__construct();

        $this->load->model("comments");
        $this->load->model("users");
        $this->load->library("api");
        $this->load->helper("util");

        if ($this->session->comment_sort == null){
            $this->session->comment_sort = 0;
        }
    }


    public function getall(){
        $post = (($this->input->get("post") === null) || (!re_integer($this->input->get("post")))) ? (-1) : ($this->input->get("post"));
        $parent = (($this->input->get("parent") === null) || (!re_integer($this->input->get("parent")))) ? (-1) : ($this->input->get("parent"));
        $page = (($this->input->get("page") === null) || (!re_integer($this->input->get("page")))) ? (0) : ((int)$this->input->get("page"));

        if ($post < 0){
            $this->_invalid("The specified post ID is not valid");
            return;
        }

        $comments = $this->comments->getAll($post, $parent, $page, $this->session->comment_sort);

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("pages", $this->comments->countPages($post, ($parent == -1) ? (null) : ($parent), $this->session->comment_sort))
                ->add("per_page", $this->comments->limit)
                ->add("current", $page)
                ->add("comments", $comments)
                ->get()
        );
    }

    public function get($id){
        $comment = $this->comments->get($id);

        if ($comment == null){
            $this->_notfound();
            return;
        }

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("comment", $comment)
                ->get()
        );
    }

    public function create(){
        if ($this->session->user == null){
            $this->_unauthorized("You must be logged in to perform this action");
            return;
        }

        if (($this->raw_input->content == null) || ($this->raw_input->parent == null) || ($this->raw_input->post == null)){
            $this->_invalid();
            return;
        }

        if (($this->raw_input->content == "") || (!re_integer($this->raw_input->parent)) || (!re_integer($this->raw_input->post)) || ($this->raw_input->post <= 0)){
            $this->_invalid();
            return;
        }

        $parent = ($this->raw_input->parent < 0) ? (null) : ($this->raw_input->parent);

        $comment = $this->comments->create($this->raw_input->post, $this->raw_input->content, $parent, $this->session->user->id);

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("message", "Comment successfuly created")
                ->add("comment", $comment)
                ->get()
        );
    }

    public function update($id){
        if ($this->session->user == null){
            $this->_unauthorized();
            return;
        }

        if (
            (($this->raw_input->content == null) || ($this->raw_input->content == "")) ||
            (($id == null) || ($id < 0) || (!re_integer($id)))
        ){
            $this->_invalid();
            return;
        }

        $comment = $this->comments->get($id);

        if ($comment == null){
            $this->_notfound();
            return;
        }

        if ($comment->user_id != $this->session->user->id){
            $this->_unauthorized();
            return;
        }

        $this->comments->update($id, $this->raw_input->content);

        $this->_success("Comment updated successfuly");
    }

    public function delete($id){
        if ($this->session->user == null){
            $this->_unauthorized();
            return;
        }

        $comment = $this->comments->get($id);

        if ($comment == null){
            $this->_notfound();
            return;
        }

        if ($comment->user_id != $this->session->user_id){
            $this->_unauthorized();
            return;
        }

        $this->comments->delete($id);

        $this->_success("Comment successfuly deleted");
    }


    public function usercomments($user_id){
        $page = (($this->input->get("page") === null) || (!re_integer($this->input->get("page")))) ? (0) : ($this->input->get("page"));
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("pages", $this->comments->countPagesUser($user_id))
                ->add("per_page", $this->comments->limit)
                ->add("current", (int)$page)
                ->add("comments", $this->comments->getAllUser($page, $user_id))
                ->get()
        );
    }


    public function getvote($comment_id){
        $vote = null;
        if ($this->session->user != null){
            $vote = $this->comments->getVote($comment_id, $this->session->user->id);
        }

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("vote", ($vote == null) ? (array("vote" => 0)) : ($vote))
                ->get()
        );
    }

    public function putvote($comment_id){
        if (($this->raw_input->vote === null) || (!re_integer($this->raw_input->vote))){
            $this->_invalid();
            return;
        }

        if ($this->session->user == null){
            $this->_unauthorized();
            return;
        }

        $comment = $this->comments->get($comment_id);
        if ($comment == null){
            $this->_notfound();
            return;
        }

        $vote = $this->raw_input->vote;
        if ($vote > 1) { $vote =  1; }
        if ($vote < -1){ $vote = -1; }

        $v = $this->comments->putVote($comment_id, $this->session->user->id, $vote);
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("vote", $v)
                ->add("message", "Vote saved")
                ->get()
        );
    }


    public function getsort(){
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("sort", array_map(
                    function($k, $v){
                        return array(
                            "id"        => $k,
                            "title"     => $v->title,
                            "active"    => ($this->session->comment_sort == $k) ? (true) : (false)
                        );
                    },
                    array_keys($this->comments->sortModes),
                    $this->comments->sortModes
                ))->get()
        );
    }

    public function getSortCurrent(){
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("sort", array(
                    "id"    => $this->comments->sortModes[$this->session->comment_sort]->id,
                    "title" => $this->comments->sortModes[$this->session->comment_sort]->title
                ))->get()
        );
    }

    public function putsort(){
        if (($this->input->post("id") == null) || (!in_array($this->input->post("id"), array_keys($this->posts->sortModes)))){
            $this->_invalid();
        }

        $this->session->comment_sort = $this->input->post("id");
        $this->_success("Sort mode updated");
    }


    public function generate(){
        $this->comments->_generate();
        $this->_success();
    }

    
}
