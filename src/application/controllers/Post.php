<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends EXT_Controller {
    public function __construct(){
        parent::__construct();

        $this->load->model("posts");
        $this->load->model("users");
        $this->load->library("api");
        $this->load->helper("util");

        if ($this->session->post_sort == null){
            $this->session->post_sort = 0; 
        }
    }


    public function getall(){
        $page = (($this->input->get("page") === null) || (!re_integer($this->input->get("page")))) ? (0) : ((int)$this->input->get("page"));
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("pages", $this->posts->countPages($this->session->post_sort))
                ->add("per_page", $this->posts->limit)
                ->add("current", $page)
                ->add("posts", $this->posts->getAll($page, $this->session->post_sort))
                ->get()
        );
    }

    public function get($id){
        $post = $this->posts->get($id);
        if ($post == null){
            $this->_notfound();
            return;
        }

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("post", $post)
                ->get()
        );
    }

    public function create(){
        if ($this->session->user == null){
            $this->_unauthorized("You must be logged in to perform this action");
            return;
        }
        
        if (($this->raw_input->title == null) || ($this->raw_input->content == null)){
            $this->_invalid();
            return;
        }

        $title = htmlspecialchars($this->raw_input->title);
        $content = htmlspecialchars($this->raw_input->content);

        $result = $this->posts->create($title, $content, $this->session->user->id);
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("message", "Post successfuly created")
                ->add("post", $result)
                ->get()
        );
    }

    public function update($id){
        $post = $this->posts->get($id);
        if ($post == null){
            $this->_notfound("The specified post could not be found");
            return;
        }

        if (($this->session->user == null) || ($this->session->user->id != $post->id)){
            $this->_unauthorized();
            return;
        }

        if (($this->input->post("title") == null) || ($this->input->post("content") == null)){
            $this->_invalid();
            return;
        }

        $this->posts->update($id, $title, $content);
        $this->_success("The post was successfuly edited");
    }

    public function delete($id){
        $post = $this->posts->get($id);
        if ($post == null){
            $this->_notfound("The specified post could not be found");
            return;
        }

        if (($this->session->user->id == null) || ($post->user_id != $this->session->user->id)){
            $this->_unauthorized();
            return;
        }

        $this->posts->delete($id);
        $this->_success("The post was successfuly deleted");
    }


    public function userposts($user_id){
        $page = (($this->input->get("page") === null) || (!re_integer($this->input->get("page")))) ? (0) : ($this->input->get("page"));
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("pages", $this->posts->countPagesUser($user_id))
                ->add("per_page", $this->posts->limit)
                ->add("current", (int)$page)
                ->add("posts", $this->posts->getAllUser($page, $user_id))
                ->get()
        );
    }


    public function getvote($post_id){
        $vote = null;
        if ($this->session->user != null){
            $vote = $this->posts->getVote($post_id, $this->session->user->id);
            if ($vote != null){
                $vote->vote = (int)$vote->vote;
            }
        }

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("vote", ($vote == null) ? (array("vote" => 0)) : ($vote))
                ->get()
        );
    }

    public function putvote($post_id){
        if (($this->raw_input->vote === null) || (!re_integer($this->raw_input->vote))){
            $this->_invalid();
            return;
        }

        if ($this->session->user == null){
            $this->_unauthorized();
            return;
        }

        $post = $this->posts->get($post_id);
        if ($post == null){
            $this->_notfound();
            return;
        }

        $vote = $this->raw_input->vote;
        if ($vote > 1) { $vote =  1; }
        if ($vote < -1){ $vote = -1; }

        $v = $this->posts->putVote($post_id, $this->session->user->id, $vote);
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("vote", $v)
                ->add("message", "Vote saved")
                ->get()
        );
    }


    public function getsort(){
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("sort", array_map(
                    function($k, $v){
                        return array(
                            "id"        => $k,
                            "title"     => $v->title,
                            "active"    => ($this->session->post_sort == $k) ? (true) : (false)
                        );
                    },
                    array_keys($this->posts->sortModes),
                    $this->posts->sortModes
                ))->get()
        );
    }

    public function getSortCurrent(){
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("sort", array(
                    "id"    => $this->posts->sortModes[$this->session->post_sort]->id,
                    "title" => $this->posts->sortModes[$this->session->post_sort]->title
                ))->get()
        );
    }

    public function putsort(){
        if (($this->input->post("id") == null) || (!in_array($this->input->post("id"), array_keys($this->posts->sortModes)))){
            $this->_invalid();
        }

        $this->session->post_sort = $this->input->post("id");
        $this->_success("Sort mode updated");
    }


    public function generate(){
        $this->posts->_generatePost();
        $this->_success("Post successfuly created");
    }

    public function simulate(){
        $this->load->model("users");

        $usersTotal = $this->users->countAll();
        $i = 0;
        $users = array();
        while ($i <= $usersTotal){
            $us = $this->users->getAll($i);
            foreach ($us as $u){ $users[] = $u->id; }
            $i += $this->users->limit;
        }

        $postsTotal = $this->posts->countPages(0);
        for ($i = 0; $i < $postsTotal; ++$i){
            $posts = $this->posts->getAll($i, 0);
            foreach ($posts as $p){
                echo ".";
                flush();
                foreach ($users as $u){
                    if (mt_rand(0,100) > 35){
                        $this->posts->putVote($p->id, $u, (mt_rand(0,1) == 0) ? (-1) : (1));
                    }
                }
            }
        }
    }
}
