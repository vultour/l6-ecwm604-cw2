<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends EXT_Controller {
    public function __construct(){
        parent::__construct();

        $this->load->model("users");
        $this->load->library("api");
    }


    public function login(){
        if (($this->input->post("username") == null) || ($this->input->post("password") == null)){
            $this->_invalid();
            return;
        }

        $user = $this->users->login($this->input->post("username"), $this->input->post("password"));

        if ($user === false){
            $this->_unauthorized("Invalid username or password");
            return;
        }

        $this->session->user = $user;

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("user", $this->users->getUserProfile($user->id))
                ->get()
        );
    }

    public function logout(){
        $this->session->user = NULL;
        $this->_success("Successfuly logged out");
    }


    public function getall(){
        $offset = (($this->input->get("offset") === null) || (!preg_match("/[1-9]+[0-9]*/", $this->input->get("offset")))) ? (0) : ($this->input->get("offset"));
        $this->_blankview(
            $this->api
                ->response(200)
                ->add("total", $this->users->countAll())
                ->add("per-page", $this->users->limit)
                ->add("offset", $offset)
                ->add("users", $this->users->getAll($offset))
                ->get()
        );
    }

    public function get($id){
        $user = $this->users->get($id);
        if ($user == null){
            $this->_notfound();
            return;
        }

        $this->_blankview(
            $this->api
                ->response(200)
                ->add("user", $user)
                ->get()
        );
    }

    public function create(){
        if ($this->session->user != null){
            $this->_unauthorized("You already have an account");
            return;
        }

        if (($this->raw_input->username == null) || ($this->raw_input->password == null)){
            $this->_invalid();
            return;
        }

        $username = htmlspecialchars($this->raw_input->username);

        if ($this->users->getName($username) != null){
            $this->_message(409, "The specified username already exists");
            return;
        }

        $this->users->create($username, $this->raw_input->password);
        $this->_success("User successfuly created");
    }

    public function update($id){
        if (($this->session->user == null) || ($this->session->user->id != $id)){
            $this->_unauthorized();
            return;
        }

        if ($this->raw_input->password == null){
            $this->_invalid();
            return;
        }

        $this->users->update($id, $this->raw_input->password);
        $this->_success("User details updated");
    }

    public function delete($id){
        if (($this->session->user == null) || ($this->session->user->id != $id)){
            $this->_unauthorized();
            return;
        }

        $this->users->delete($id);
        $this->session->user = null;
        $this->_success("User successfuly deleted");
    }


    public function current(){
        if ($this->session->user == null){
            $this->_notfound("No user is logged in");
            return;
        }

        return $this->_blankview(
            $this->api
                ->response(200)
                ->add("user", $this->users->getUserProfile($this->session->user->id))
                ->get()
        );
    }


    public function generate(){
        $this->users->_generateLogins();
        $this->_success("Users sucessfuly created");
    }
}
