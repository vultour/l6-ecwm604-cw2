<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RawInput extends stdClass{
    public $_raw;
    private $_data;
    public function __get($key){ return (isset($this->_data->$key)) ? ($this->_data->$key) : (null); }
    public function __construct($data){
            $data = trim($data, '"');
            $this->_raw = $data;
            $this->_data = json_decode($data);
    }
}

class EXT_Controller extends CI_Controller {
    public $raw_input;
	
    public function __construct(){
        parent::__construct();

        $this->raw_input = new RawInput($this->input->raw_input_stream);
    }


    public function loggedIn(){
        return ($this->session->user == null) ? (false) : (true);
    }


	public function _blankview($content){
        $this->load->view("blank", array("content" => $content));
    }

    public function _message($status, $message){
        $this->_blankview(
            $this->api
                ->response($status)
                ->add("message", $message)
                ->get()
        );
    }

    public function _unauthorized($message = "You are not authorized to perform this action"){
        $this->_message(403, $message);
    }

    public function _success($message = "Operation successful"){
        $this->_message(200, $message);
    }

    public function _invalid($message = "The supplied data is invalid"){
        $this->_message(400, $message);
    }

    public function _notfound($message = "The requested resource could not be found"){
        $this->_message(404, $message);
    }
}
