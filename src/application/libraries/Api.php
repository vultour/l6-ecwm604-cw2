<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiResponse{
    private $data;
    public function __construct(){ $this->data = new stdClass(); }
    public function add($key, $value){ $this->data->$key = $value; return $this; }
    public function get(){ return json_encode($this->data, JSON_PRETTY_PRINT); }
}

class Api{
    public function response($status){
        return (new ApiResponse())->add("status", $status);
    }
}