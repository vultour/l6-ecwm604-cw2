<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debug{
    public $usernames = ["james","john","robert","michael","mary","william","david","richard","charles","joseph","thomas","patricia","christopher","linda","barbara","daniel","paul","mark","elizabeth","donald","jennifer","george","maria","kenneth","susan","steven","edward","margaret","brian","ronald","dorothy","anthony","lisa","kevin","nancy","karen","betty","helen","jason","matthew","gary","timothy","sandra","jose","larry","jeffrey","frank","donna","carol","ruth","scott","eric","stephen","andrew","sharon","michelle","laura","sarah","kimberly","deborah","jessica","raymond","shirley","cynthia","angela","melissa","brenda"];

    public function getLipsum($amount, $start){
        return simplexml_load_file("http://www.lipsum.com/feed/xml?amount=$amount&start=$start")->lipsum;
    }
}