<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gui{
    private $CI = NULL;
    private $commentFormat = '
        <div class="col s12 comment" id="comment-%5$d">
            <div class="row">
                <div class="col s12 comment-top">%2$s on %3$s | <a data-parent-id="%5$d" data-post-id="%6$d" href="#!" class="comment-reply">reply</a></div>
                <div class="col s12 comment-content">%1$s</div>
                <div class="col s12">%4$s</div>
            </div>
        </div>
    ';

    public function __construct(){
        $this->CI =& get_instance();
    }

    public function generateCommentChain($comment_id){
        $current = $this->CI->posts->getComment($comment_id);
        $next = $this->CI->posts->getCommentChildrenIDs($comment_id);

        //var_dump($current);

        $children = "";
        foreach ($next->result() as $comment){
            $children .= $this->generateCommentChain($comment->id);
        }

        return sprintf(
            $this->commentFormat,
            $current->content,
            $current->user,
            $current->posted_at,
            $children,
            $current->id,
            $current->post_id
        );
    }
}
