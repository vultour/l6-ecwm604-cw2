<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiResponse{
    private $data;
    public function __construct(){ $this->data = new stdClass(); }
    public function add($key, $value){ $this->data->$key = $value; return $this; }
    public function get(){ return json_encode($this->data); }
}

class Api{
    public function response($status){
        return (new ApiResponse())->add("status", $status);
    }

    public function re_integer($x){
        return (preg_match("/[1-9][0-9]*/", $x) != false);
    }    
}