<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends CI_Model{
    public $sortModes = [];
    public $limit = 8; 

    public function __construct(){
        parent::__construct();

        // $this->load->library("stdclass_helper");

        $this->sortModes = [
            (object)[
                "title"     => "Date (newest first)",
                "getFunc"   => "get_dateDesc",
                "countFunc" => "count_all"
            ],
            (object)[
                "title"     => "Date (oldest first)",
                "getFunc"   => "get_dateAsc",
                "countFunc" => "count_all"
            ]
        ];
    }


    public function get($id){
        $prefix = DATABASE_PREFIX;
        $comment = $this->db->query("
            SELECT
                c.*,
                u.username,
                (SELECT COUNT(*) AS children FROM ${prefix}comments WHERE parent = c.id) AS children
            FROM
                ${prefix}comments AS c,
                ${prefix}users AS u
            WHERE
                c.id = ${id} AND
                u.id = c.user_id
            LIMIT 1
        ")->row();

        if ($comment == null){ return null; }

        $votes = $this->getVotes($comment->id);
        $comment->upvotes = $votes->upvotes;
        $comment->downvotes = $votes->downvotes;

        return $comment;
    }

    public function getAll($postId, $parent = null, $page = 0, $sort_id = 0){
        if (($parent < 0) || empty($parent)){ $parent = null; }
        if ($page < 0){ $page = 0; }

        $comments = call_user_func_array(
            array($this, $this->sortModes[$sort_id]->getFunc),
            array((int)$postId, $parent, $page)
        );

        if ($comments === null){ return null; }

        foreach ($comments as &$comment){
            $votes = $this->getVotes($comment->id);
            $comment->upvotes = $votes->upvotes;
            $comment->downvotes = $votes->downvotes;
        }

        return $comments;
    }

    public function getAllUser($page, $user_id){
        if ($page < 0){ $page = 0; }

        $comments = $this->db
            ->where("user_id", $user_id)
            ->order_by("id", "DESC")
            ->limit($this->limit, ($page * $this->limit))
            ->get("comments")
            ->result();

        if ($comments === null){ return null; }

        foreach ($comments as &$comment){
            $votes = $this->getVotes($comment->id);
            $user = $this->users->get($comment->user_id)->username;
            $children = $this->getChildrenCount($comment->id);
            $comment->children = $children;
            $comment->username = $user;
            $comment->upvotes = $votes->upvotes;
            $comment->downvotes = $votes->downvotes;
        }

        return $comments;
    }

    public function create($post_id, $content, $parent, $user_id){
        $this->db->insert(
            "comments",
            array(
                "post_id"   => $post_id,
                "content"   => $content,
                "parent"    => $parent,
                "user_id"   => $user_id,
               // "posted_at" => date("Y-m-d H:i:s")
            )
        );

        return $this->get(
            $this->db
                ->where("user_id", $user_id)
                ->order_by("id DESC")
                ->limit(1)
                ->get("comments")
                ->row()
                ->id
        );
    }

    public function update($id, $content){
        return $this->db->update(
            "comments",
            array(
                "id"        => $id,
                "content"   => $content,
            )
        );
    }

    public function delete($id){
        return $this->db->delete(
            "comments",
            array(
                "id" => $id
            )
        );
    }


    public function countAll($post_id, $parent, $sort_id){
        return call_user_func_array(
            array($this, $this->sortModes[$sort_id]->countFunc),
            array($post_id, $parent)
        );
    }

    public function countPages($post_id, $parent, $sort_id){
        return ceil($this->countAll($post_id, $parent, $sort_id) / $this->limit);
    }

    public function countPagesUser($user_id){
        return ceil($this->db->where("user_id", $user_id)->from("comments")->count_all_results() / $this->limit);
    }


    public function getVote($comment_id, $user_id){
        return $this->db
            ->where("user_id", $user_id)
            ->where("comment_id", $comment_id)
            ->get("votes_comments")
            ->row();
    }

    public function putVote($comment_id, $user_id, $vote){
        $this->db->replace("votes_comments", array(
            "comment_id"    => $comment_id,
            "user_id"       => $user_id,
            "vote"          => $vote
        ));

        return $this->getVote($comment_id, $user_id);
    }


    public function get_dateDesc($postId, $parent, $page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $limit);
        $parent = ($parent === null) ? ($parent = "IS NULL") : ("= ${parent}");
        return $this->db->query("
            SELECT
                c.*,
                u.username,
                (SELECT COUNT(*) AS children FROM ${prefix}comments WHERE parent = c.id) AS children
            FROM
                ${prefix}comments AS c,
                ${prefix}users AS u
            WHERE
                c.post_id = ${postId} AND
                c.parent ${parent} AND
                u.id = c.user_id
            ORDER BY c.id DESC
            LIMIT ${offset}, ${limit}
        ")->result();
    }

    public function get_dateAsc($postId, $parent, $page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $limit);
        $parent = ($parent === null) ? ($parent = "IS NULL") : ("= ${parent}");
        return $this->db->query("
            SELECT
                c.*,
                u.username,
                (SELECT COUNT(*) AS children FROM ${prefix}comments WHERE parent = c.id) AS children
            FROM
                ${prefix}comments AS c,
                ${prefix}users AS u
            WHERE
                c.post_id = ${postId} AND
                c.parent ${parent} AND
                u.id = c.user_id
            ORDER BY c.id ASC
            LIMIT ${offset}, ${limit}
        ")->result();
    }


    public function count_all($post_id, $parent){
        return $this->db
            ->where("post_id", $post_id)
            ->where("parent", $parent)
            ->from("comments")
            ->count_all_results();
    }

    public function countPosts_popularityDay(){
        $prefix = DATABASE_PREFIX;
        return $this->db->query("SELECT COUNT(*) AS cnt FROM ${prefix}posts WHERE UNIX_TIMESTAMP(posted_at) > (UNIX_TIMESTAMP() - 86400)")->result()[0]->cnt;
    }

    public function vote($post_id, $user, $vote){
        $this->db->replace(
            "votes",
            array(
                "post_id" => $post_id,
                "user" => $user,
                "vote" => $vote
            )
        );
    }

    public function getVotes($id){
        $prefix = DATABASE_PREFIX;
        $output = $this->db->query("
            SELECT * FROM
                (SELECT COUNT(vote) as upvotes FROM ${prefix}votes_comments WHERE vote > 0 AND comment_id = $id) as u,
                (SELECT COUNT(vote) as downvotes FROM ${prefix}votes_comments WHERE vote < 0 AND comment_id = $id) as d
        ")->row();

        $output->upvotes = (int)$output->upvotes;
        $output->downvotes = (int)$output->downvotes;

        return $output;
    }

    public function getUserKarma($user_id){
        $prefix = DATABASE_PREFIX;
        return $this->db->query("
        SELECT
            SUM(s.sum) AS sum
        FROM
            (SELECT
                p.*,
                (SELECT
                    SUM(vote)
                FROM
                    ${prefix}votes_comments AS v
                WHERE
                    v.comment_id = p.id
                ) AS sum
            FROM
                ${prefix}comments AS p
            WHERE
                p.user_id = ${user_id}
            ) as s;
        ")->row()->sum;
    }


    public function getChildrenCount($parent_id){
        $prefix = DATABASE_PREFIX;
        return $this->db->query("SELECT COUNT(*) AS children FROM ${prefix}comments WHERE parent = ${parent_id}")->row()->children;
    }
}
