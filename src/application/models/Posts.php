<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Model{
    public $sortModes = [];
    public $limit = 8; 

    public function __construct(){
        parent::__construct();

        $this->load->model("users");

        $this->sortModes = [
            (object)[
                "title"     => "Date (newest first)",
                "getFunc"   => "getAll_dateDesc",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "Date (oldest first)",
                "getFunc"   => "getAll_dateAsc",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "Popularity (all time)",
                "getFunc"   => "getAll_popularityAll",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "Title (A-Z)",
                "getFunc"   => "getAll_titleAsc",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "Title (Z-A)",
                "getFunc"   => "getAll_titleDesc",
                "countFunc" => "countPosts_all"
            ]
        ];
    }


    public function get($id){
        $prefix = DATABASE_PREFIX;
        $post = $this->db->query("
            SELECT p.title, p.content, p.link, p.user_id, p.posted_at, u.username
            FROM
                ${prefix}posts as p,
                ${prefix}users as u
            WHERE
                p.id = ${id} AND
                u.id = p.user_id
            LIMIT 1
        ")->row();

        if ($post == null){ return null; }

        $votes = $this->getVotes($id);
        $user = $this->users->get($post->user_id)->username;
        $post->username = $user;
        $post->upvotes = $votes->upvotes;
        $post->downvotes = $votes->downvotes;

        return $post;
    }

    public function getAll($page = 0, $sort_id){
        $posts = call_user_func_array(
            array($this, $this->sortModes[$sort_id]->getFunc),
            array($page)
        );

        if ($posts == null){ return null; }

        foreach ($posts as &$post){
            $votes = $this->getVotes($post->id);
            $user = $this->users->get($post->user_id)->username;
            $post->username = $user;
            $post->upvotes = $votes->upvotes;
            $post->downvotes = $votes->downvotes;
        }

        return $posts;
    }

    public function getAllUser($page, $user_id){
        $posts = $this->db
            ->where("user_id", $user_id)
            ->order_by("posted_at", "DESC")
            ->limit($this->limit, ($page * $this->limit))
            ->get("posts")
            ->result();

        if ($posts == null){ return null; }

        foreach ($posts as &$post){
            $votes = $this->getVotes($post->id);
            $user = $this->users->get($post->user_id)->username;
            $post->username = $user;
            $post->upvotes = $votes->upvotes;
            $post->downvotes = $votes->downvotes;
        }

        return $posts;
    }

    public function create($title, $content, $user_id){
        $isLink = $this->_isLink($content);
        $this->db->insert(
            "posts",
            array(
                "title"     => $title,
                "content"   => $content,
                "user_id"   => $user_id,
                "posted_at" => date("Y-m-d H:i:s"),
                "link"      => $isLink
            )
        );
        return $this->db
            ->select("id, title")
            ->where("user_id", $user_id)
            ->order_by("id DESC")
            ->limit(1)
            ->get("posts")
            ->row();
    }

    public function update($id, $title, $content){
        $isLink = $this->_isLink($content);
        return $this->db->update(
            "posts",
            array(
                "id"        => $id,
                "title"     => $title,
                "content"   => $content,
                "posted_at" => date("Y-m-d H:i:s"),
                "link"      => $isLink
            )
        );
    }

    public function delete($id){
        return $this->db->delete(
            "posts",
            array(
                "id" => $id
            )
        );
    }


    public function countAll($sort_id){
        return call_user_func(array($this, $this->sortModes[$sort_id]->countFunc)); //$this->$func();
    }

    public function countPages($sort_id){
        return ceil($this->countAll($sort_id) / $this->limit);
    }

    public function countPagesUser($user_id){
        return ceil($this->db->where("user_id", $user_id)->from("posts")->count_all_results() / $this->limit);
    }

    
    public function getVote($post_id, $user_id){
        return $this->db
            ->where("user_id", $user_id)
            ->where("post_id", $post_id)
            ->get("votes")
            ->row();
    }

    public function putVote($post_id, $user_id, $vote){
        $this->db->replace("votes", array(
            "post_id"      => $post_id,
            "user_id"   => $user_id,
            "vote"      => $vote
        ));

        return $this->getVote($post_id, $user_id);
    }


    public function getAll_dateDesc($page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $this->limit);
        return $this->db->query("
            SELECT id, title, SUBSTRING(content, 1, 512) as content, link, posted_at, user_id
            FROM
                ${prefix}posts
            ORDER BY posted_at DESC
            LIMIT ${offset}, ${limit}
        ")->result();
    }

    public function getAll_dateAsc($page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $this->limit);
        return $this->db->query("
            SELECT id, title, SUBSTRING(content, 1, 512) as content, link, posted_at, user_id
            FROM
                ${prefix}posts
            ORDER BY posted_at ASC
            LIMIT ${offset}, ${limit}
        ")->result();
    }

    public function getAll_popularityAll($page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $this->limit);
        return $this->db->query("
            SELECT p.id, p.title, SUBSTRING(p.content, 1, 512) as content, p.link, p.user_id, p.posted_at, v.votes
            FROM
                ${prefix}posts as p,
                (SELECT post_id, SUM(vote) as votes FROM ${prefix}votes GROUP BY post_id) as v
            WHERE
                p.id = v.post_id
            ORDER BY v.votes DESC
            LIMIT ${offset}, ${limit}
        ")->result();
    }

    public function getAll_titleDesc($page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $this->limit);
        return $this->db->query("
            SELECT id, title, SUBSTRING(content, 1, 512) as content, link, posted_at, user_id
            FROM
                ${prefix}posts
            ORDER BY title DESC
            LIMIT ${offset}, ${limit}
        ")->result();
    }

    public function getAll_titleAsc($page){
        $prefix = DATABASE_PREFIX;
        $limit = $this->limit;
        $offset = ($page * $this->limit);
        return $this->db->query("
            SELECT id, title, SUBSTRING(content, 1, 512) as content, link, posted_at, user_id
            FROM
                ${prefix}posts
            ORDER BY title ASC
            LIMIT ${offset}, ${limit}
        ")->result();
    }


    public function countPosts_all(){
        return $this->db->count_all("posts");
    }

    public function countPosts_popularityDay(){
        $prefix = DATABASE_PREFIX;
        return $this->db->query("SELECT COUNT(*) AS cnt FROM ${prefix}posts WHERE UNIX_TIMESTAMP(posted_at) > (UNIX_TIMESTAMP() - 86400)")->row()->cnt;
    }

    public function vote($post_id, $user, $vote){
        $this->db->replace(
            "votes",
            array(
                "post_id" => $post_id,
                "user" => $user,
                "vote" => $vote
            )
        );
    }

    public function getVotes($post_id){
        $prefix = DATABASE_PREFIX;
        $output = $this->db->query("
            SELECT * FROM
                (SELECT COUNT(vote) as upvotes FROM ${prefix}votes WHERE vote > 0 AND post_id = $post_id) as u,
                (SELECT COUNT(vote) as downvotes FROM ${prefix}votes WHERE vote < 0 AND post_id = $post_id) as d
        ")->row();

        $output->upvotes = (int)$output->upvotes;
        $output->downvotes = (int)$output->downvotes;

        return $output;
    }


    public function getUserPosts($username){
        return $this->db->where("posted_by", $username)
            ->order_by("posted_at", "DESC")
            ->get("posts");
    }

    public function getUserVotes($username){
        return $this->db->where("user", $username)
            ->order_by("voted_at", "DESC")
            ->get("votes");
    }

    public function getUserVote($post_id, $username){
        $output = $this->db->where("user", $username)
            ->where("post_id", $post_id)
            ->limit(1)
            ->get("votes")->result();

        return (count($output) > 0) ? ((int)$output[0]->vote) : (0);
    }

    public function getUserKarma($user_id){
        return $this->db->query("
        SELECT
            SUM(s.sum) AS sum
        FROM
            (SELECT
                p.*,
                (SELECT
                    SUM(vote)
                FROM
                    ecwm604_cw2_votes AS v
                WHERE
                    v.post_id = p.id
                ) AS sum
            FROM
                ecwm604_cw2_posts AS p
            WHERE
                p.user_id = ${user_id}
            ) as s;
        ")->row()->sum;
    }

    public function getUserPostsCount($username){
        $this->db->where("posted_by", $username);
        $this->db->from("posts");
        return $this->db->count_all_results();
    }

    public function getUserVotesCount($username){
        $this->db->where("user", $username);
        $this->db->where("vote !=", 0);
        $this->db->from("votes");
        return $this->db->count_all_results();
    }


    public function getComment($comment_id){
        return $this->db->where("id", $comment_id)
            ->limit(1)
            ->get("comments")
            ->result()[0];
    }

    public function getTopLevelComments($post_id){
        return $this->db->where("post_id", $post_id)
            ->where("parent", NULL)
            ->get("comments");
    }

    public function getCommentChildrenIDs($comment_id){
        return $this->db->select("id")
            ->where("parent", $comment_id)
            ->get("comments");
    }

    public function putComment($post_id, $user, $parent, $content){
        $this->db->insert(
            "comments",
            array(
                "post_id"   => $post_id,
                "parent"    => (($parent == NULL) || (trim($parent) == "")) ? (NULL) : ($parent),
                "user"      => $user,
                "content"   => htmlspecialchars($content),
                "posted_at" => date("Y-m-d H:i:s")
            )
        );
    }


    public function hlp_pagesFromTotal($total){
        $pages = ceil($total / POSTS_PER_PAGE);
        return ($pages < 1) ? (1) : ($pages);
    }

    public function hlp_getGravatarUrl($username, $size){
        return sprintf("https://www.gravatar.com/avatar/%s?s=%d&d=identicon&f=y", md5(sprintf("%s@westminster.ac.uk", $username)), $size);
    }


    private function _isLink($content){
        return !(
            filter_var(
                trim($content),
                FILTER_VALIDATE_URL,
                FILTER_FLAG_ENCODE_LOW || FILTER_FLAG_ENCODE_HIGH || FILTER_FLAG_HOST_REQUIRED
            )
        ) === false;
    }

    public function _generatePost(){
        $this->load->library("debug");
        $this->load->model("users");

        $good = false;
        while (!$good){
            $s = explode(".", $this->debug->getLipsum(mt_rand(1, 10), mt_rand(1, 100)));
            if (count($s) < 1){ return; }
            $link = true;
            if (mt_rand(0, 1) == 0){
                if (count($s) < 3){ continue; }
                $content = implode(".", array_splice($s, 2));
                $link = false;
            } else{
                $url_parts = explode(" ", trim(strtolower($s[1])));
                if (count($url_parts) < 3){ continue; }
                $content = vsprintf("http://%s-%s.%s", array_splice($url_parts, 0, 3));
            }
            $good = true;
        }

        $this->db->insert(
            "posts",
            array(
                "title" => trim($s[0]),
                "link" => $link,
                "posted_at" => date("c"),
                "user_id" => $this->users->getName($this->debug->usernames[mt_rand(0, count($this->debug->usernames)-1)])->id,
                "content" => htmlspecialchars($content)
            )
        );
    }

    public function _simulateVoting($voteChance){
        $this->load->library("debug");
        $total = $this->getTotal();
        $pages = $this->hlp_pagesFromTotal($total);
        $grandTotal = ($total * count($this->debug->usernames));

        $x = 0;
        foreach ($this->debug->usernames as $user){
            for ($page = 1; $page <= $pages; ++$page){
                $posts = $this->getPosts($page);
                foreach ($posts->result() as $post){
                    ++$x;

                    if ((1.0 * mt_rand() / getrandmax()) <= $voteChance){
                        $this->vote($post->id, $user, (mt_rand(0, 1) == 0) ? (1) : (-1));
                    }

                    echo ".";

                    if (($x % 10) == 0){
                        if (($x % 150) == 0){
                            echo "STILL WORKING<br>";
                        }

                        ob_flush();
                        flush();
                    }
                }
            }
        }
        echo "<br><br>";
        printf("<a href='%s'>============ DONE ============</a>", site_url(""));
    }
}
