<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model{
    private $saltLength = 16;
    public $limit   = 8;

    public function __construct(){
        parent::__construct();
    }


    public function login($username, $password){
        $user = $this->getName($username);
        if ($user == null){ return false; }        
        if ($this->hashPassword($password, $user->salt) != $user->password){ return false; }

        return $user;
    }


    public function getAll($offset = 0){
        return $this->db
            ->select(array("id", "username"))
            ->order_by("username", "ASC")
            ->limit($this->limit, $offset)
            ->get("users")
            ->result();
    }

    public function get($id){
        $this->load->model("posts");
        $this->load->model("comments");
        $user = $this->db
            ->select(array("id", "username"))
            ->where("id", $id)
            ->limit(1)
            ->get("users")
            ->row();

        if ($user == null){ return null; }

        $profile = $this->getUserProfile($user->id);
        $user->postCount = $profile->posts;
        $user->commentCount = $profile->comments;
        $user->voteCount = $profile->votes;
        $user->userhash = $profile->userhash;

        $user->postKarma = $this->posts->getUserKarma($user->id);
        $user->commentKarma = $this->comments->getUserKarma($user->id);

        return $user;
    }

    public function getName($username){
        return $this->db
            ->where("username", $username)
            ->limit(1)
            ->get("users")
            ->row();
    }

    public function create($username, $password){
        $salt = $this->createSalt();
        return $this->db->insert(
            "users",
            array(
                "username"  => $username,
                "password"  => $this->hashPassword($password, $salt),
                "salt"      => $salt
            )
        );
    }

    public function update($id, $password){
        $salt = $this->createSalt();
        return $this->db->update(
            "users",
            array(
                "password"  => $this->hashPassword($password, $salt),
                "salt"      => $salt
            ),
            array("id" => $id)
        );
    }

    public function delete($id){
        return $this->db->delete(
            "users",
            array(
                "id" => $id
            )
        );
    }


    public function getUserProfile($id){
        $prefix = DATABASE_PREFIX;
        return $this->db->query("
            SELECT id, username, MD5(username) AS userhash, p.cnt AS posts, c.cnt AS comments, (vp.cnt + vc.cnt) AS votes
            FROM
                ${prefix}users,
                (SELECT COUNT(*) AS cnt FROM ${prefix}posts WHERE user_id = $id) AS p,
                (SELECT COUNT(*) AS cnt FROM ${prefix}comments WHERE user_id = $id) AS c,
                (SELECT COUNT(*) AS cnt FROM ${prefix}votes WHERE user_id = $id) AS vp,
                (SELECT COUNT(*) AS cnt FROM ${prefix}votes_comments WHERE user_id = $id) AS vc
            WHERE id = $id
            LIMIT 1
        ")->row();
    }


    public function countAll(){
        return $this->db->count_all("users");
    }


    private function createSalt(){
        return bin2hex(openssl_random_pseudo_bytes($this->saltLength / 2));
    }

    private function hashPassword($password, $salt){
        return hash("sha256", $password.$salt);
    }


    public function _generateLogins(){
        $this->load->library("debug");
        $password = "pwd";

        foreach ($this->debug->usernames as $u){
            $this->create($u, $password);
        }
    }
}
