        <script src="<?php echo URL_BASE; ?>/assets/js/local-settings.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/jquery/jquery-2.1.1.min.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/materialize/js/materialize.min.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/underscore/underscore.min.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/backbone/backbone.min.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/js/util.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/app.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/user.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/menu.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/index.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/submit.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/post.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/app/userpage.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/js/coursework.js"></script>
        <script>
            $(function(){
                <?php
                    if ($this->session->errors){
                        foreach ($this->session->errors as $error){
                            printf("Materialize.toast('<span class=\"toast-error\">%s</span>', 4000);\n", $error);
                        }
                    }

                    if ($this->session->notifications){
                        foreach ($this->session->notifications as $notification){
                            printf("Materialize.toast('<span class=\"toast-notification\">%s</span>', 4000);\n", $notification);
                        }
                    }
                ?>
            });
        </script>
    </body>
</html>
