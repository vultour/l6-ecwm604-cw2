<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Coursework 2</title>
        <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="<?php echo URL_BASE; ?>/assets/materialize/css/materialize.min.css" rel="stylesheet">
        <link href="<?php echo URL_BASE; ?>/assets/css/coursework.css" rel="stylesheet">
    </head>
    <body>
        <div id="container" class="container">
            <div id="menu"></div>
