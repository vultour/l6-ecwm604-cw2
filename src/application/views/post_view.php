            <div class="row" style="margin-right:0;">
                <div class="col s12 m3">
                    <h5>Sort by:</h5>
                    <ul class="collection">
                    <a class="collection-item" href="<?php echo previousPage(); ?>">&lt; Back</a>
                    </ul>
                    <p>You are logged in as:</p>
                    <div class="user-info-box">
                        <h3><?php echo ($this->session->username) ? ($this->session->username) : ("username"); ?></h3>
                        <img src="<?php echo $this->posts->hlp_getGravatarUrl($this->session->username, 120); ?>">
                        <p>You have posted <strong><?php echo $userPosts; ?></strong> post<?php echo ($userPosts == 1)?(""):("s"); ?> and casted <strong><?php echo $userVotes; ?></strong> vote<?php echo ($userVotes == 1)?(""):("s"); ?>!</p>
                        <p><a href="<?php echo site_url("/logout"); ?>"><i class="material-icons" style="vertical-align:middle;">settings_power</i> Logout</a></p>
                    </div>
                </div>
                <div class="col s12 m9" style="padding:0;">
                    <div class="row">
                        <div class="col s12 post">
                            <div class="row no-space">
                                <div class="post-title col s12"><?php echo $post->title; ?></div>
                                <div class="col s12">
                                    <div class="row no-space">
                                        <div class="col s4 post-votes right-align">
                                            <i class="material-icons vote-arrow vote-up green-text <?php if ($post->uservote != 1){ echo "text-lighten-3"; } ?>" data-id="<?php echo $post->id; ?>">call_made</i>
                                            <span class="item-vote-total"><?php echo $post->votes; ?></span>
                                            <i class="material-icons vote-arrow vote-down red-text <?php if ($post->uservote != -1){ echo "text-lighten-3"; } ?>" data-id="<?php echo $post->id; ?>">call_received</i>
                                        </div>
                                        <div class="col s8 valign">
                                            <span class="valign" style="display:block;"><?php printf("by <a href='%s'>%s</a> on %s", site_url(sprintf("/user/%s", $post->user)), $post->user, date("d/M/Y", strtotime($post->date))); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-content col s12">
                                <?php if ($post->link): ?>
                                    <a href="<?php echo $post->content; ?>"><?php echo $post->content; ?></a>
                                <?php else: ?>
                                    <?php foreach (explode("\n", $post->content) as $paragraph): ?>
                                        <p><?php echo $paragraph; ?></p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 comments" id="comments">
                            <h4>Comments:</h4>
                            <div class="row">
                                <div class="col s12 add-coment">
                                    <form name="add-comment" action="<?php echo site_url("/submit/comment"); ?>" method="post">
                                        Submitting as: <?php echo $this->session->username; ?>
                                        <textarea name="comment">Enter your comment here</textarea>
                                        <input type="hidden" name="post_id" value="<?php echo $post->id; ?>">
                                        <input type="submit" value="Submit">
                                    </form>
                                </div>
                                <?php
                                    foreach ($comments->result() as $comment){
                                        echo $this->gui->generateCommentChain($comment->id);
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
