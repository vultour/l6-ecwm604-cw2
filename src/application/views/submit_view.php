            <div class="row" style="margin-right:0;">
                <div class="col s12 m3">
                    <h5>Sort by:</h5>
                    <ul class="collection">
                    <a class="collection-item" href="<?php echo previousPage(); ?>">&lt; Back</a>
                    </ul>
                    <p>You are logged in as:</p>
                    <div class="user-info-box">
                        <h3><?php echo ($this->session->username) ? ($this->session->username) : ("username"); ?></h3>
                        <img src="<?php echo $this->posts->hlp_getGravatarUrl($this->session->username, 120); ?>">
                        <p>You have posted <strong><?php echo $userPosts; ?></strong> post<?php echo ($userPosts == 1)?(""):("s"); ?> and casted <strong><?php echo $userVotes; ?></strong> vote<?php echo ($userVotes == 1)?(""):("s"); ?>!</p>
                        <p><a href="<?php echo site_url("/logout"); ?>"><i class="material-icons" style="vertical-align:middle;">settings_power</i> Logout</a></p>
                    </div>
                </div>
                <div class="col s12 m9" style="padding:0;">
                    <div class="row">
                        <div class="col s12"><h4>Submit post</h4></div>
                        <form class="col s12 post-submit-form" method="post" action="<?php echo site_url("/submit/post"); ?>">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input placeholder="Post title" name="title" type="text" class="validate">
                                    <label for="first_name">Title</label>
                                </div>
                                <div class="input-field col s6">
                                    <input disabled id="username" name="username" type="text" class="validate" value="<?php echo $this->session->username; ?>">
                                    <label for="first_name">Username</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea class="materialize-textarea" name="content"></textarea>
                                    <label for="last_name">Content</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="submit" value="Submit" class="btn waves-effect green">
                                    <a class="btn waves-effect red" href="<?php echo previousPage(); ?>">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
