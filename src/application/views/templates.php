        </div>
        <div class="loader valign-wrapper"><div class="loader-image valign"><img src="assets/img/loader.gif"></div></div>

        <script type="text/x-template" id="template-userbox">
            <% if (logged_in){ %>
                <h3><%= username %></h3>
                <img src="<%= gravatar_url(userhash) %>">
                <p>You have posted <strong><%= posts %></strong> posts, <%= comments %> comments, and cast <strong><%= votes %></strong> votes!</p>
                <p><a href="#user/<%= id %>">Profile</a> | <a class="logout">Logout <i class="material-icons" style="vertical-align:middle;">settings_power</i></a></p>
            <% } else{ %>
                <h4>You are currently not logged in!</h4>
            <% } %>
        </script>

        <script type="text/x-template" id="template-sortmodes">
            <h5>Sort by:</h5>
            <ul class="collection sort-modes"></ul>
        </script>

        <script type="text/x-template" id="template-postlist">
            <div class="item-vote-section">
                <div class="row valign-wrapper">
                    <span class="item-vote-total col s6 valign" style="display:block"><%= (post.upvotes - post.downvotes) %></span>
                    <div class="item-vote-arrows col s6">
                        <i class="material-icons <% if (user.logged_in){ %>green-text text-lighten-3 <% } else{%>grey-text text-lighten-1 <% } %>vote-up" data-id="<%= post.id %>">call_made</i>
                        <i class="material-icons <% if (user.logged_in){ %>red-text text-lighten-3 <% } else{%>grey-text text-lighten-1 <% } %>vote-down" data-id="<%= post.id %>">call_received</i>
                    </div>
                </div>
            </div>
            <div class="item-main-section">
                <div class="row">
                    <div class="item-main-title col s12">
                        <% if (post.link != 0){ %>
                            <a href="<%= post.content %>"><%= post.title %></a> <span>(external)</span>
                        <% } else{ %> 
                            <a href="#post/<%= post.id %>"><%= post.title %></a> <span>(textpost)</span>
                        <% } %>
                    </div>
                    <div class="item-main-sub col s12">
                        <a href="#post/<%= post.id %>">comment</a>
                        - posted at <%= post.posted_at %> by <a href="#user/<%= post.user_id %>"><%= post.username %></a>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/x-template" id="template-pagination">
            <div class="row">
                <div class="col s12 center">
                    <ul class="pagination">
                        <!--<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>-->
                        <% if (current <= 0){ %>
                            <li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
                        <% } else{ %>
                            <li class="pagination-item"><a href="#<%= (current - 1) %>"><i class="material-icons">chevron_left</i></a></li>
                        <% } %>

                        <% for (var i = 0; i < total; ++i){ %>
                            <li class="<% if (i  == current){ %>active<% } else{ %>pagination-item<% } %>"><a href="#<%= i %>"><%= (i + 1) %></a></li>
                        <% } %>

                        <% if (current >= (total - 1)){ %>
                            <li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>
                        <% } else{ %>
                            <li class="pagination-item"><a href="#<%= (current + 1) %>"><i class="material-icons">chevron_right</i></a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </script>

        <script type="text/x-template" id="template-submit">
            <div class="col s12 m9" style="padding:0;">
                <div class="row">
                    <div class="col s12"><h4>Submit post</h4></div>
                    <form class="col s12 post-submit-form" method="post" action="#">
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="title" type="text" class="validate" id="field-title">
                                <label for="title">Post title</label>
                            </div>
                            <div class="input-field col s12">
                                <textarea class="materialize-textarea" name="content" id="field-content"></textarea>
                                <label for="content">Content</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="post-submit" type="button" value="Submit" class="btn waves-effect green">
                                <a class="btn waves-effect red" href="#">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </script>

        <script type="text/x-template" id="template-menu">
            <nav role="navigation" class="purple darken-3" id="">
                <div class="nav-wrapper">
                    <a href="#0" class="brand-logo right">ShareIt!</a>
                    <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a href="#0">Homepage</a></li>
                        <% if (logged_in){ %>
                            <li class="purple lighten-1"><a href="#submit">Submit a post</a></li>
                        <% } %>
                        <li><a class="container-toggle">Toggle width</a></li>
                        <li><a href="#generate">Generate post</a></li>
                        <li><a href="#simulate">Simulate votes</a></li>
                        <% if (logged_in){ %>
                            <li><a class="logout">Logout <img class="img-round" style="vertical-align:middle;" src="<%= gravatar_url(userhash) %>&s=30"></a></li>
                        <% } else{ %>
                            <li><a class="login">Login / Register</a></li>
                        <% } %>
                    </ul>
                </div>
            </nav>
            <div id="modal-login" class="modal">
                <div class="modal-content">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs z-depth-1">
                                <li class="tab col s6"><a href="#tab-login" class="active tab-button-login">Login</a></li>
                                <li class="tab col s6"><a href="#tab-register" class="tab-button-register">Register</a></li>
                            </ul>
                        </div>
                        <div id="tab-login" class="col s12">
                            <h4>Login</h4>
                            <div class="row">
                                <form class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Username" id="login-username" type="text" class="validate" onkeyup="if (event.keyCode == 13){ document.getElementsByClassName('perform-login')[0].click(); }">
                                            <label for="login-username">Username:</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input placeholder="Password" id="login-password" type="password" class="validate" onkeyup="if (event.keyCode == 13){ document.getElementsByClassName('perform-login')[0].click(); }">
                                            <label for="login-password">Password:</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <div class="login-error-box center-align red lighten-4 red-text hide"></div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-register" class="col s12">
                            <h4>Register</h4>
                            <div class="row">
                                <form class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Username" id="register-username" type="text" class="validate">
                                            <label for="login-username">Username:</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input placeholder="Password" id="register-password" type="password" class="validate">
                                            <label for="login-password">Password:</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input placeholder="Confirm" id="register-password-confirm" type="password" class="validate">
                                            <label for="login-password">Confirm:</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <div class="register-error-box center-align red lighten-4 red-text hide"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="waves-effect waves-green btn-flat perform-login">Login</a>
                    <a class="waves-effect waves-purple btn-flat perform-register hide">Register</a>
                    <a class="modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                </div>
            </div>
        </script>

        <script type="text/x-template" id="template-post">
            <div class="col s12 post">
                <div class="row no-space">
                    <div class="post-title col s12"><%= post.title %></div>
                    <div class="col s12">
                        <div class="row no-space">
                            <div class="col s4 post-votes right-align">
                                <i class="material-icons vote-arrow vote-up <% if (user.logged_in){ %>green-text text-lighten-3 <% } else{%>grey-text text-lighten-1 <% } %>">call_made</i>
                                <span class="item-vote-total"><%= (post.upvotes - post.downvotes) %></span>
                                <i class="material-icons vote-arrow vote-down <% if (user.logged_in){ %>red-text text-lighten-3 <% } else{%>grey-text text-lighten-1 <% } %>">call_received</i>
                            </div>
                            <div class="col s8 valign">
                                <span class="valign" style="display:block;">posted on <%= post.posted_at %> by <%= post.username %></span>
                            </div>
                        </div>
                    </div>
                    <div class="post-content col s12">
                    <% if (post.link != 0){ %>
                        <a href="<%= post.content %>"><%= post.content %></a>
                    <% } else{ %>
                        <pre><%= post.content %></pre>
                    <% } %>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/x-template" id="template-comments">
            <h4>Comments:</h4>
            <div class="row">
                <div class="col s12 add-coment">
                    <form name="add-comment" action="#" method="post">
                        Submitting as: <%= username %>
                        <textarea name="comment" class="comment-content-textarea">Enter your comment here</textarea>
                        <input type="button" value="Submit" class="comment-submit">
                    </form>
                </div>
                <div class="col s12 comments"></div>
                <div class="col s12 load-more hide"><a class="hand">Load more comments</a></div>
            </div>
        </script>

        <script type="text/x-template" id="template-comment">
            <div class="row" style="margin-bottom:5px;">
                <div class="col s12 comment-top">
                    <div class="comment-votes">
                        <i class="material-icons vote-arrow vote-up">call_made</i>
                        <span class="vote-total"><%= (comment.upvotes - comment.downvotes) %></span>
                        <i class="material-icons vote-arrow vote-down">call_received</i>
                    </div>
                    <%= comment.username %> on <%= comment.user_id %> |
                    <a class="comment-reply">reply</a>
                </div>
                <div class="col s12 comment-content"><%= comment.content %></div>
                <div class="col s12">
                    <div class="row comment-children" style="margin-bottom:0px;"></div>
                </div>
                <div class="col s12 comment-load-more <% if (comment.children < 1){ %>hide<% } %>"><a class="hand">Load more comments</a></div>
            </div>
        </script>

        <script type="text/x-template" id="template-userpage">
            <div class="row">
                <div class="col s12">
                    <h2>User: <%= user.username %></h2>
                </div>

                <% if (current.logged_in && (current.id == user.id)){ %>
                <form class="col s12">
                    <div class="row">
                        <div class="col s12"><h4>Change password</h4></div>
                        <div class="input-field col s5">
                            <input placeholder="password" id="new-password" type="password">
                            <label for="new-password">New password</label>
                        </div>
                        <div class="input-field col s5">
                            <input placeholder="password" id="new-password-confirm" type="password">
                            <label for="new-password">Confirm password</label>
                        </div>
                        <div class="input-field col s2">
                            <button class="btn save-password">Save</button>
                        </div>
                    </div>
                </form>
                <% } %>

                <div class="col s12">
                    <div class="row">
                        <div class="col s6 user-posts">
                            <div class="row user-page-column">
                                <h4 class="col s12 center">Posts</h4>
                                <div class="col s12 center">
                                    <table class="centered">
                                        <thead>
                                            <tr>
                                                <th>Total</th>
                                                <th>Karma</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><%= user.postCount %></td>
                                                <td><%= user.postKarma %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col s12 center user-posts-pagination"></div>
                            </div>
                        </div>
                        <div class="col s6 user-comments">
                            <div class="row user-page-column">
                                <h4 class="col s12 center">Comments</h4>
                                <div class="col s12 center">
                                    <table class="centered">
                                        <thead>
                                            <tr>
                                                <th>Total</th>
                                                <th>Karma</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><%= user.commentCount %></td>
                                                <td><%= user.commentKarma %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col s12 center user-comments-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/x-template" id="template-pagination-new">
            <ul class="pagination">
                <% if (current <= 1){ %>
                    <li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
                <% } else{ %>
                    <li class="waves-effect">
                        <a data-page="<%= (current - 1) %>" class="hand pagination-item">
                            <i class="material-icons">chevron_left</i>
                        </a>
                    </li>
                <% } %>

                <% for (var i = 1; i <= pages; ++i){ %>
                <li class="waves effect <% if (i == current){ %>active<% } %>">
                    <a data-page="<%= i %>" class="hand pagination-item"><%= i %></a>
                </li>
                <% } %>

                <% if (current >= pages){ %>
                    <li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>
                <% } else{ %>
                    <li class="waves-effect"><a data-page="<%= (current + 1) %>" class="hand pagination-item"><i class="material-icons">chevron_right</i></a></li>
                <% } %>
            </ul>
        </script>