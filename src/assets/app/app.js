var app = {};


app.updateVoteArrows = function(x){
    if (x.logged_in){
        x.up.removeClass("grey-text text-lighten-1");
        x.down.removeClass("grey-text text-lighten-1");
        x.up.addClass("green-text text-lighten-3");
        x.down.addClass("red-text text-lighten-3");

        x.up.removeClass("text-darken-3");
        x.up.addClass("green-text text-lighten-3");
        x.down.removeClass("text-darken-3");
        x.down.addClass("red-text text-lighten-3");

        if (x.vote > 0){
            x.up.removeClass("text-lighten-3");
            x.up.addClass("text-darken-3");
        } else if (x.vote < 0){
            x.down.removeClass("text-lighten-3");
            x.down.addClass("text-darken-3");
        }
    } else{
        x.up.removeClass("green-text text-lighten-3 text-darken-3");
        x.down.removeClass("red-text text-lighten-3 text-darken-3");
        x.up.addClass("grey-text text-lighten-1");
        x.down.addClass("grey-text text-lighten-1");
    }
}

app.getVoteDirection = function(el){
    var direction = 0;
    if (el.hasClass("text-lighten-3")){
        direction = 1
    } else{
        direction = 0;
    }

    if (el.hasClass("red-text")){
        direction = (direction * (-1));
    }

    return direction;
}


app.Router = Backbone.Router.extend({
    routes: {
        "post/:post":   "getPost",
        "user/:user":   "getUser",
        "submit":       "submit",
        "*page":        "getPage"
    },
    view: null,
    loadView: function(v){
        if (this.view != null){ this.view.detach(); }
        this.view = v;
    }
});


app.router = new app.Router();


app.router.on("route:getPage", function(page){
    $(".loader").show();
    if (!is_int(page)){ app.router.navigate("" + app.pagination.attributes.current); }
    else{ app.pagination.update({current: page}); }
    this.loadView(new app.IndexView());
});

app.router.on("route:submit", function(){
    this.loadView(new app.SubmitView());
});

app.router.on("route:getPost", function(post){
    $(".loader").show();
    if (!is_int(post)){ window.location.href = URL_BASE; }
    else{
        this.loadView(new app.PostPageView({model: new app.Post({id: post}, {collection: new app.PostList()})}));
    }
});

app.router.on("route:getUser", function(user){
    $(".loader").show();
    if (!is_int(user)){ window.location.href = URL_BASE; }
    else{
        this.loadView(new app.UserPageView({model: new app.User({id: user})}));
    }
});