app.Post = Backbone.Model.extend({
    urlRoot: URL_BASE + "/api/posts",
    initialize: function(){
        if (this.id){
            this.vote = new app.PostVote({id: this.id});
            this.listenTo(this.vote, "change", this.voteChanged);
            this.vote.fetch();
        }
    },
    parse: function(data){
        if (data.status){
            if (data.status == 200){
                return data.post;
            } else{
                Materialize.toast("Error: " + data.message + "(status " + data.status + ")");
                return {error: "Couldn't find post data in server response"};
            }
        } else{
            return data;
        }
    },
    voteChanged: function(){
        this.fetch();
    }
});
app.Vote = Backbone.Model.extend({
    defaults: {
        vote: 0
    },
    parse: function(data){
        if (data.status == 200){
            if (data.vote){
                data.vote.vote_id = data.vote.id;
                if (data.vote.post_id){ data.vote.id = data.vote.post_id; }
                if (data.vote.comment_id){ data.vote.id = data.vote.comment_id; }
                return data.vote
            }
            return data;
        } else{
            Materialize.toast("Error retrieving vote state (" + data.status + ": " + data.message + ")", 3500);
        }
    }
})

app.PostVote = app.Vote.extend({
    urlRoot: URL_PATH + "/api/posts/vote"
});

app.PostList = Backbone.Collection.extend({
    model: app.Post,
    url: URL_BASE + "/api/posts",
    parse: function(data){
        this.trigger("paginationUpdate", {current: data.current, total: data.pages, per_page: data.per_page});
        return data.posts;
    }
});
app.PostsView = Backbone.View.extend({
    tagName: "div",
    className: "col s12 hoverable item",
    template: _.template($("#template-postlist").html()),
    initialize: function(){
        this.listenTo(app.user, "change", this.loggedInStatusChanged);
        this.listenTo(this.model.vote, "change", this.voteChanged);
        this.listenTo(this.model, "change", this.modelChanged);
    },
    render: function(){
        this.$el.html(this.template({post: this.model.toJSON(), vote: this.model.vote.toJSON(), user: app.user.toJSON()}));

        var t = this;
        this.$el.children(".item-vote-section").children().children(".item-vote-arrows").children().on("click", function(e){
            if (!app.user.attributes.logged_in){
                Materialize.toast("You must be logged in to vote!", 3500);
                return;
            }

            var direction = app.getVoteDirection($(e.target));

            t.model.vote.save(
                {vote: direction},
                {
                    success: function(m, r, o){
                        if (r.status == 200){
                            t.model.vote.fetch();
                        } else{
                            Materialize.toast("Error: " + r.message + "(status " + r.status + ")");
                        }
                    },
                    error: function(){
                        alert("Error connecting to the server");
                    }
                }
            );
        });
        return this;
    },
    loggedInStatusChanged: function(){
        var arrows = this.$el.children(".item-vote-section").children().children(".item-vote-arrows");
        var up = arrows.children(".vote-up");
        var down = arrows.children(".vote-down");

        app.updateVoteArrows({up: up, down: down, vote: this.model.vote.get("vote"), logged_in: app.user.get("logged_in")});

        this.model.vote.fetch();
    },
    modelChanged: function(){
        this.$el.children(".item-vote-section").children().children(".item-vote-total").text(
            (this.model.attributes.upvotes - this.model.attributes.downvotes)
        );
    },
    voteChanged: function(){
        var arrows = this.$el.children(".item-vote-section").children().children(".item-vote-arrows");
        var up = arrows.children(".vote-up");
        var down = arrows.children(".vote-down");

        app.updateVoteArrows({up: up, down: down, logged_in: app.user.get("logged_in"), vote: this.model.vote.get("vote")});
    },
    detach: function(){
        this.remove();
    }
});


app.SortMode = Backbone.Collection.extend({
    defaults: {
        title: "Unset",
        active: false
    }
})

app.SortModes = Backbone.Collection.extend({
    //model: app.SortMode,
    initialize: function(){
        
    },
    parse: function(data){
        if (data.status == 200){
            return data.sort;
        } else{
            Materialize.toast("Error retrieving sort modes: " + data.message + "(status " + data.status + ")");
        }
    },
    setMode: function(id){
        var t = this;
        console.log(t.url);
        $.post(
            t.url,
            {id: id},
            function(data){
                if (data.status == 200){ t.fetch({reset: true}); }
                else{ Materialize.toast("Sort mode error: " + data.message + "(status" + data.status + ")"); }
            },
            "json"
        );
    }
});

app.SortModesView = Backbone.View.extend({
    template: _.template($("#template-sortmodes").html()),
    initialize: function(){
        this.views = [];
        this.collection = new (app.SortModes.extend({url: this.model.url}))();
        //this.listenTo(this.collection, "update", this.add);
        this.listenTo(this.collection, "reset update", this.render);
        this.collection.fetch();
        this.render();
    },
    render: function(){
        $(".sort-modes-container").children().remove();
        $(".sort-modes-container").append(this.$el.html(this.template()));
        _.each(this.collection.models, function(m){ this.add(m); }, this);
        $(".sort-modes-container").find(".collection-item").on("click", this.sortModeClick.bind(this));
    },
    add: function(sortMode){
        this.$el.find(".sort-modes").append(
            '<a class="collection-item hand' + 
            ((sortMode.get("active")) ? (" active") : ("")) + 
            '" data-id="' + sortMode.id + '">' + sortMode.get("title") + '</a>'
        );
    },
    sortModeClick: function(e){
        var sort = $(e.currentTarget).data("id");
        console.log("Switching to mode:", sort);
        this.collection.setMode(sort);
    },
    detach: function(){
        _.each(this.views, function(v){ v.detach(); });
        this.remove();
    }
});


app.GenericPagination = Backbone.View.extend({
    template: _.template($("#template-pagination-new").html()),
    initialize: function(){
        if (this.model == null){
            this.model = new (Backbone.Model.extend({defaults:{current:1,pages:1,per_page:0}}))();
        }

        this.listenTo(this.model, "change", this.render);
        this.render();
    },
    render: function(){
        this.$el.html(
            this.template(this.model.toJSON())
        );

        this.$el.find(".pagination-item").on("click", this.click.bind(this));
    },
    click: function(e){
        var page = $(e.currentTarget).data("page");
        console.log("Target:", e);
        console.log("Setting:", page);
        this.model.set({current: page});
    }
});


app.Pagination = Backbone.Model.extend({
    defaults: {
        current: 0,
        total: 0,
        per_page: 1
    },
    update: function(attr){
        this.set(attr);
    },
    getPages: function(){
        return Math.ceil(this.attributes.total / this.attributes.limit);
    },
    getPage: function(){
        return Math.floor(this.attributes.offset / this.attributes.limit) + 1;
    }
});
app.PaginationView = Backbone.View.extend({
    template: _.template($("#template-pagination").html()),
    initialize: function(){
        this.listenTo(this.model, "change", this.render);
    },
    render: function(){
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    detach: function(){
        this.remove();
    }
});


app.pagination = new app.Pagination();


app.IndexView = Backbone.View.extend({
    tagName: "div",
    //el: "#content-container",
    pagination: null,
    initialize: function(){
        app.postList = new app.PostList();
        app.postList.on("add", this.add, this);
        app.postList.on("reset", this.reset, this);
        app.postList.on("paginationUpdate", this.updatePagination, this);

        this.sortModes = new app.SortModesView({model: {url: URL_BASE + "/api/posts/sort"}});
        this.listenTo(this.sortModes.collection, "add reset change", this.fetch.bind(this));

        this.fetch();
        this.render();
    },
    render: function(){
        $("#content-container").children().remove();
        $("#content-container").append('<div id="posts-container" class="row"></div>');
        $("#content-container").append((new app.PaginationView({model: app.pagination})).render().el);
        $(".sort-modes-container").append()
    },
    events: {},
    offset: 0,
    fetch: function(){
        app.postList.fetch({reset: true,  data: {page: app.pagination.get("current")}});
    },
    add: function(post){
        $("#posts-container").append((new app.PostsView({model: post})).render().el);
    },
    reset: function(){
        $("#posts-container").children().remove();
        $(".loader").fadeOut(1500);
        app.postList.each(this.add, this);
    },
    updatePagination: function(e){
        app.pagination.update(e);
    },
    detach: function(){
        this.remove();
    }
});


//app.indexView = new app.IndexView();