app.MenuView = Backbone.View.extend({
    template: _.template($("#template-menu").html()),
    model: app.user,
    events: {
        "click .login": "openLogin",
        "click .perform-login": "performLogin",
        "click .perform-register": "performRegister",
        "click .logout": "confirmLogout",
        "click .container-toggle": "toggleContainer",
        "click .tab-button-login": "focusLogin",
        "click .tab-button-register": "focusRegister"
    },
    initialize: function(){
        this.listenTo(this.model, "change", this.render);
        $("#menu").children().remove();
        this.render();
    },
    render: function(){
        $("#menu").append(
            this.$el.html(this.template(this.model.toJSON()))
        );
        this.$el.find("ul.tabs").tabs();
    },
    openLogin: function(){
        this.$el.find("#modal-login").openModal();
        this.$el.find("#login-username").focus();
        this.$el.find("ul.tabs").tabs();
    },
    performLogin: function(){
        this.model.login(
            $("#login-username").val(),
            $("#login-password").val(),
            function(result){
                if (result !== true){
                    var eb = $(".login-error-box");
                    eb.text(result);
                    eb.removeClass("hide");
                } else{
                    $("#modal-login").closeModal();
                }
            }
        );
    },
    performRegister: function(){
        var username = this.$el.find("#register-username").val();
        var password = this.$el.find("#register-password").val();
        var eb = this.$el.find(".register-error-box");
        var t = this;

        if (password != this.$el.find("#register-password-confirm").val()){
            eb.text("Passwords don't match!");
            eb.removeClass("hide");
            return;
        }

        var newUser = new (app.User.extend({parse: function(data){ return data; }}))();
        newUser.save({
            username: username,
            password: password
        },{
            success: function(m, r, o){
                if (r.status != 200){
                    eb.text(r.message);
                    eb.removeClass("hide");
                } else{
                    Materialize.toast("User successfuly registered!", 3500);
                    t.$el.find(".tab-button-login").click();
                }
            }
        });
    },
    confirmLogout: function(){
        if (window.confirm("Are you sure you want to logout")){
            this.model.logout();
        }
    },
    toggleContainer: function(){
        $("#container").toggleClass("container");
    },
    focusLogin: function(){
        this.$el.find(".perform-login").removeClass("hide");
        this.$el.find(".perform-register").addClass("hide");
        this.$el.find("#login-username").focus();
    },
    focusRegister: function(){
        this.$el.find(".perform-register").removeClass("hide");
        this.$el.find(".perform-login").addClass("hide");
        this.$el.find("#register-username").focus();
    },
    detach: function(){
        this.remove();
    }
});

app.menuView = new app.MenuView();