//app.SinglePost = app.Post.extend({});

app.Comment = Backbone.Model.extend({
    urlRoot: URL_BASE + "/api/comments",
    initialize: function(){
        if (this.id){
            this.vote = new app.CommentVote({id: this.id});
            this.listenTo(this.vote, "change", this.voteChanged);
            this.vote.fetch();
        }
    },
    parse: function(data){
        if (data.status){
            if (data.status == 200){
                return data.comment;
            } else{
                Materialize.toast("Error: " + data.message + "(status " + data.status + ")");
                return {error: "Couldn't find post data in server response"};
            }
        } else{
            return data;
        }
    },
    voteChanged: function(){
        this.fetch();
    }
});

app.CommentVote = app.Vote.extend({
    urlRoot: URL_PATH + "/api/comments/vote"
});

app.Comments = Backbone.Collection.extend({
    model: app.Comment,
    url: URL_BASE + "/api/comments",
    initialize: function(){
        this.pagination = {current: 0, max: 0};
    },
    parse: function(data){
        this.pagination.current = data.current;
        this.pagination.max = data.pages;
        this.trigger("paginationUpdate", this.pagination);
        return data.comments;
    },
    _fetch: function(post, parent, page){
        this.fetch({data: {post: post, parent: parent, page: page}});
    },
    hasMore: function(){
        return (this.pagination.current < (this.pagination.max - 1));
    }
});

app.submitComment = function(post, parent, content, callback){
    var comment = new (app.Comment.extend({urlRoot: URL_PATH + "/api/comments"}))();


    comment.save(
        {content: content, post: post, parent: parent},
        {
            success: function(model, data){
                if (callback != null){
                    callback(data);
                }
            },
            error: function(){
                alert("Connection failed");
            }
        }
    );
}

app.PostView = Backbone.View.extend({
    model: null,
    tagName: "div",
    template: _.template($("#template-post").html()),
    initialize: function(){
        this.listenTo(this.model, "change", this.modelChanged);
        this.listenTo(this.model.vote, "change", this.modelChanged);
        this.listenTo(app.user, "change", this.modelChanged);
        console.log("Model X:", this.model.vote.get("vote"));
        this.render();
    },
    render: function(){
        this.$el.html(this.template({post: this.model.toJSON(), user: app.user.toJSON()}));
        this.$el.find(".vote-arrow").on("click", this.arrowClicked.bind(this));
        this.modelChanged();
        return this;
    },
    arrowClicked: function(e){
        if (!app.user.get("logged_in")){
            Materialize.toast("You need to be logged in to vote", 3500);
            return;
        }
        var direction = app.getVoteDirection($(e.target));
        var t = this;

        this.model.vote.save(
            {vote: direction},
            {
                success: function(m, r, o){
                    if (r.status == 200){
                        t.model.vote.fetch();
                    } else{
                        Materialize.toast("Error: " + r.message + "(status " + r.status + ")");
                    }
                },
                error: function(){
                    alert("Error connecting to the server");
                }
            }
        );
    },
    modelChanged: function(){
        this.$el.find(".item-vote-total").text(this.model.get("upvotes") - this.model.get("downvotes"));
        this.updateVotes();
    },
    updateVotes: function(){
        var up = this.$el.find(".vote-up");
        var down = this.$el.find(".vote-down");
        console.log("XX:", {up: up, down: down, vote: this.model.vote.get("vote"), logged_in: app.user.get("logged_in")});
        app.updateVoteArrows({up: up, down: down, vote: this.model.vote.get("vote"), logged_in: app.user.get("logged_in")});
    },
    detach: function(){
        this.remove();
    }
});

app.CommentView = Backbone.View.extend({
    tagName: "div",
    className: "col s12 comment",
    //model: null,
    //collection: null,
    template: _.template($("#template-comment").html()),
    initialize: function(){
        this.depth = (this.collection == null) ? (0) : (this.collection.depth);
        this.collection = new app.Comments();
        this.collection.on("add", this.add, this);
        this.collection.on("reset", this.reset, this);
        this.collection.on("paginationUpdate", this.updatePagination, this);
        this.model.on("change", this.updateVotes, this);
        this.model.vote.on("change", this.updateVotes, this);
        this.listenTo(app.user, "change", this.updateVotes);
        this.comments = [];

        if (this.depth < 2){
            this.collection._fetch(this.model.attributes.post_id, this.model.id, 0);
        } else{
            this.collection.pagination.current = -1;
        }
    },
    render: function(){
        this.$el.html(this.template({comment: this.model.toJSON(), vote: this.model.vote.toJSON(), user: app.user.toJSON()}));
        this.getInnerElements().children(".comment-top").children(".comment-reply").on("click", this.openReply.bind(this));
        this.getInnerElements().children(".comment-load-more").children().on("click", this.loadMore.bind(this));

        this.getInnerElements().children(".comment-top").children(".comment-votes").children("i").on("click", this.arrowClicked.bind(this));

        this.updateVotes();

        return this;
    },
    add: function(comment){
        var tmp = new app.CommentView({model: comment, collection: {depth: (this.depth + 1)}});
        this.comments.push(tmp);
        this.getInnerElements().children().children(".comment-children").append(tmp.render().el);
    },
    reset: function(){
        _.each(this.comments, function(c){ c.detach(); });
        this.getInnerElements().children().children(".comment-children").children().remove();
    },
    loadMore: function(){
        this.getInnerElements().children(".comment-load-more").children("a").append("... loading");
        this.collection._fetch(this.model.attributes.post_id, this.model.id, (this.collection.pagination.current + 1));
    },
    getInnerElements: function(){
        return this.$el.children();
    },
    openReply: function(){
        if (this.getInnerElements().children(".comment-reply-box").length < 1){
            this.getInnerElements().children(".comment-content").after(
                '<div class="col s12 comment-reply-box"><textarea class="comment-reply-textarea"></textarea><button class="comment-reply-submit">Submit</button><button class="comment-reply-cancel">Cancel</button></div>'
            );

            var t = this;
            this.getInnerElements().children(".comment-reply-box").children(".comment-reply-submit").on("click", function(){
                app.submitComment(t.model.attributes.post_id, t.model.id, t.getInnerElements().children(".comment-reply-box").children(".comment-reply-textarea").val(), function(d){
                    if (d.status == 200){
                        t.collection.add(d.comment);
                        Materialize.toast(d.message, 3500);
                        t.getInnerElements().children(".comment-reply-box").remove();
                    } else{
                        Materialize.toast("Error: " + d.message + "(status " + d.status + ")", 3500);
                    }
                })
            });
            this.getInnerElements().children(".comment-reply-box").children(".comment-reply-cancel").on("click", function(){ t.getInnerElements().children(".comment-reply-box").remove(); });
        }
    },
    updatePagination: function(e){
        if (this.collection.hasMore()){
            this.getInnerElements().children(".comment-load-more").removeClass("hide");
        } else{
            this.getInnerElements().children(".comment-load-more").addClass("hide");
        }
    },
    arrowClicked: function(e){
        if (!app.user.get("logged_in")){
            Materialize.toast("You need to be logged in to vote", 3500);
            return;
        }
        var direction = app.getVoteDirection($(e.target));
        var t = this;

        this.model.vote.save(
            {vote: direction},
            {
                success: function(m, r, o){
                    if (r.status == 200){
                        t.model.vote.fetch();
                    } else{
                        Materialize.toast("Error: " + r.message + "(status " + r.status + ")");
                    }
                },
                error: function(){
                    alert("Error connecting to the server");
                }
            }
        );
    },
    updateVotes: function(){
        var arrows = this.getInnerElements().children(".comment-top").children(".comment-votes");
        var up = arrows.find(".vote-up");
        var down = arrows.find(".vote-down");
        console.log("Current text:", arrows.children(".vote-total").text());
        console.log("Model:", this.model);
        arrows.children(".vote-total").text(this.model.get("upvotes") - this.model.get("downvotes"));
        app.updateVoteArrows({up: up, down: down, vote: this.model.vote.get("vote"), logged_in: app.user.get("logged_in")});
    },
    detach: function(){
        this.reset();
        this.remove();
    }
});

app.CommentsView = Backbone.View.extend({
    tagName: "div",
    className: "col s12",
    id: "comments",
    collection: null,
    template: _.template($("#template-comments").html()),
    comments: [],
    events: {
        "click .comment-submit": "submitComment"
    },
    initialize: function(){
        this.collection = new app.Comments();
        this.collection.on("add", this.add, this);
        this.collection.on("reset", this.reset, this);
        this.collection.on("paginationUpdate", this.updatePagination, this);
        this.collection._fetch(this.model.id, -1, 0);

        this.render();
    },
    render: function(){
        this.$el.html(this.template(this.model.toJSON()));
        this.$el.children(".row").children(".load-more").children().on("click", this.loadMore.bind(this));
        return this;
    },
    add: function(comment){
        this.$el.find(".comments").append(
            new app.CommentView({model: comment}).render().el
        );
    },
    reset: function(){
        _.each(this.comments, function(c){ c.detach(); });
        this.$el.find(".comments").children().remove();
        _.each(this.comments, function(c){ this.add(c); }, this);
    },
    submitComment: function(e){
        var t = this;
        app.submitComment(this.model.id, -1, this.$el.find(".comment-content-textarea").val(), function(d){
            if (d.status == 200){
                t.collection.add(d.comment);
                Materialize.toast("Comment successfuly created!", 3500);
                console.log("Comment submitted successfuly!", d);
            } else{
                Materialize.toast("Error: " + d.message + " (status " + d.status + ")", 3500);
            }
        });
    },
    updatePagination: function(e){
        console.log("Update:", this.collection);
        if (this.collection.hasMore()){
            this.$el.children(".row").children(".load-more").removeClass("hide");
        } else{
            this.$el.children(".row").children(".load-more").addClass("hide");
        }
    },
    loadMore: function(){
        console.log("Using:", this.collection.pagination);
        this.$el.children(".row").children(".load-more").children("a").append("... loading");
        this.collection._fetch(this.model.id, -1, (this.collection.pagination.current + 1));
    },
    detach: function(){
        this.sortModes.detach();
        this.remove();
    }
});

app.PostPageView = Backbone.View.extend({
    model: null,
    initialize: function(){
        this.listenTo(this.model, "change", this.render);
        $("#content-container").children().remove();

        this.model.fetch();
    },
    render: function(){
        console.log("Model:", this.model);
        $("#content-container").append(
            new app.PostView({model: this.model.clone()}).el
        );
        $("#content-container").append(
            new app.CommentsView({model: this.model.clone()}).el
        );
        $(".loader").fadeOut(1500);
    },
    detach: function(){
        this.remove();
    }
});