app.SubmitView = Backbone.View.extend({
    template: _.template($("#template-submit").html()),
    events:{
        "click #post-submit": "submitPost"
    },
    initialize: function(){
        $("#content-container").children().remove();
        this.render();
    },
    render: function(){
        console.log(this.model);
        $("#content-container").append(
            this.$el.html(this.template())
        );
        $(".loader").fadeOut(1500);
    },
    submitPost: function(e){
        var m = new (app.Post.extend({urlRoot: URL_PATH + "/api/posts/"}))();
        console.log(m.save(
            {title: $("#field-title").val(), content: $("#field-content").val()},
            {
                success: function(model, data){
                    if (data.status == 200){
                        Materialize.toast("Post successfuly created!", 3500);
                        window.location.href = URL_BASE + "/#post/" + data.post.id;
                        console.log("Post submitted successfuly!", data);
                    } else{
                        Materialize.toast("Error: " + data.message + " (status " + data.status + ")", 3500);
                    }
                },
                error: function(){
                    alert("Error connecting to the server!");
                }
            }
        ));
        //console.log("Model:", this.model.save());
    },
    detach: function(){
        this.remove();
    }
});