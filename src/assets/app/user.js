app.User = Backbone.Model.extend({
    urlRoot: URL_BASE + "/api/users",
    defaults: {
        logged_in: false
    },
    parse: function(data){
        if (data.status == 200){
            return data.user;
        } else{
            Materialize.toast("Error retrieving user data: " + data.message + "(status " + data.status + ")");
        }
    },
    login: function(username, password, callback){
        var model = this;
        $.post(
            URL_BASE + "/api/users/login",
            {
                username: username,
                password: password
            },
            function(){},
            "json"
        ).done(function(data){
            if (data.status == 200){
                data.user.logged_in = true;
                callback(true);
                model.set(data.user);
            } else{
                callback(data.message);
            }
        }).error(function(data){
            alert("Could not connect to the server");
        });
    },
    logout: function(){
        if (!this.attributes.logged_in){ return true; }
        var model = this;
        $.get(
            URL_BASE + "/api/users/logout"
        ).done(function(data){
            model.clear({silent: true});
            model.set({logged_in: false});
        }).error(function(data){
            alert("Could not connect to the server")
        });
    },
    loadCurrent: function(){
        var model = this;
        $.get(
            URL_BASE + "/api/users/current",
            {},
            function(data){
                if (data.status == 200){
                    data.user.logged_in = true;
                    model.set(data.user);
                }
            },
            "json"
        );
    }
});


app.user = new app.User();
app.user.loadCurrent();


app.UserBoxView = Backbone.View.extend({
    template: _.template($("#template-userbox").html()),
    model: app.user,
    events: {
        "click .logout": "confirmLogout"
    },
    initialize: function(){
        this.listenTo(this.model, "change", this.render);
        $("#user-info-box").children().remove();
        this.render();
    },
    render: function(){
        $("#user-info-box").append(
            this.$el.html(this.template(this.model.toJSON()))
        );
    },
    confirmLogout: function(){
        if (window.confirm("Are you sure you want to logout")){
            this.model.logout();
        }
    }
});


app.userBoxView = new app.UserBoxView();