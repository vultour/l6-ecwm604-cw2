app.userPage = {};

app.userPage.Collection = Backbone.Collection.extend({
    url: "NO_MODEL_PASSED_IN",
    initialize: function(){
        if (this.model.id){
            this.url = URL_BASE + "/api/users/" + this.model.id + "/" + this.model.section;
            this.section = this.model.section;
            this.model = this.model.model;
        }
    },
    parse: function(data){
        if (data.status == 200){
            this.trigger("updatePagination", {current: data.current + 1, pages: data.pages, per_page: data.per_page});
            if (this.section == "posts"){
                return data.posts;
            } else if (this.section == "comments"){
                return data.comments;
            }
        } else{
            Materialize.toast("Error retrieving data: " + data.message + "(status " + data.status + ")");
        }
    },
    _fetch: function(page){
        this.fetch({reset: true, data: {page: page}});
    }
});

app.UserPageView = Backbone.View.extend({
    //el: "#content-container",
    className: "col s12",
    template: _.template($("#template-userpage").html()),
    initialize: function(){
        this.views = {};
        this.views.posts = [];
        this.views.comments = [];

        this.postsPagination = new app.GenericPagination();
        this.commentsPagination = new app.GenericPagination();

        this.posts = new app.userPage.Collection(null, {model: {id: this.model.id, section: "posts", model: app.Post}});
        this.comments = new app.userPage.Collection(null, {model: {id: this.model.id, section: "comments", model: app.Comment}});

        this.listenTo(this.model, "change", this.render);
        this.listenTo(app.user, "change", this.render);

        this.listenTo(this.posts, "updatePagination", function(e){ this.postsPagination.model.set(e)});
        this.listenTo(this.comments, "updatePagination", function(e){ this.commentsPagination.model.set(e)});

        this.listenTo(this.posts, "add", this.addPost);
        this.listenTo(this.posts, "reset", this.resetPosts);
        this.listenTo(this.comments, "add", this.addComment);
        this.listenTo(this.comments, "reset", this.resetComments);

        this.listenTo(this.postsPagination.model, "change", function(){ this.posts._fetch(this.postsPagination.model.get("current") - 1); });
        this.listenTo(this.commentsPagination.model, "change", function(){ this.comments._fetch(this.commentsPagination.model.get("current") - 1); });

        this.model.fetch();
    },
    render: function(){
        this.$el.html(this.template({user: this.model.toJSON(), current: app.user.toJSON()}));

        $("#content-container").children().remove();
        $("#content-container").append(this.el);

        this.$el.find(".user-posts-pagination").append(this.postsPagination.el);
        this.$el.find(".user-comments-pagination").append(this.commentsPagination.el);

        this.posts._fetch(0);
        this.comments._fetch(0);

        this.$el.find(".save-password").on("click", (function(event){
            var password = this.$el.find("#new-password").val();
            if (password != this.$el.find("#new-password-confirm").val()){
                Materialize.toast("The passwords don't match!");
                return;
            }
            this.model.save({password: password}, {
                success: function(model, data){
                    Materialize.toast(data.message, 3500);
                }
            });
        }).bind(this));

        window.setTimeout(function(){ $(".loader").fadeOut(1500); }, 500);
    },
    addPost: function(post){
        var tmp = new app.PostsView({model: post});
        this.views.posts.push(tmp);
        tmp.render();
        this.$el.find(".user-posts-pagination").before(
            tmp.el
        );
    },
    resetPosts: function(){
        _.each(this.views.posts, function(v){ v.detach(); }, this);
        this.$el.find(".user-posts").find(".item").remove();
        this.posts.each(this.addPost, this);
    },
    addComment: function(comment){
        var tmp = new app.CommentView({model: comment, collection: {depth: 3}});
        this.views.comments.push(tmp);
        tmp.render();
        console.log("View:", tmp);
        console.log("El:", this.$el.find(".user-comments-pagination"));
        this.$el.find(".user-comments-pagination").before(
            tmp.el
        );
    },
    resetComments: function(){
        _.each(this.views.comments, function(v){ v.detach(); }, this);
        this.$el.find(".user-comments").find(".comment").remove();
        this.comments.each(this.addComment, this);
    },
    detach: function(){
        this.resetPosts();
        this.resetComments();
        this.posts = [];
        this.comments = [];
        this.remove();
    }
});