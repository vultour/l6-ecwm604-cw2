function gravatar_url(userhash){
    return "https://www.gravatar.com/avatar/" + userhash + "?f=y&d=identicon";
}

function is_int(x){
    return /^([0-9])|([1-9][0-9]*)$/.test(String(x));
}