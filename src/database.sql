DROP TABLE IF EXISTS `ecwm604_cw2_comments`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw2_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `posted_at` datetime NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `post_id` (`post_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `ecwm604_cw2_posts`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw2_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) COLLATE utf8_bin NOT NULL,
  `posted_at` datetime NOT NULL,
  `link` tinyint(1) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `posted_at` (`posted_at`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `ecwm604_cw2_votes`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw2_votes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `vote` smallint(6) NOT NULL,
  `voted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`post_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `ecwm604_cw2_votes_comments`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw2_votes_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `vote` smallint(3) NOT NULL,
  `voted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `comment_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `ecwm604_cw2_users`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw2_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;