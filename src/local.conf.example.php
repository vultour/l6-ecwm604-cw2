<?php

define("URL_BASE",          "http://coursework1.ecwm604.io" );
define("SHORT_URLS",        false                           );

define("DATABASE_HOST",     "localhost"                     );
define("DATABASE_USER",     "username"                      );
define("DATABASE_PASS",     "password"                      );
define("DATABASE_NAME",     "dbname"                        );
